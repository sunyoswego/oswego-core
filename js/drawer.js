
(function($) {

  $('.drawer').each(function(index, el) {
    $(el).hide();
  });
  var delay = 500;
  var open = false;
  var $navItem, current, drawerTimerIn, drawerTimerOut;

  $('.global-nav-item').hover(function() {
    $navItem = $(this);
    // window.clearTimeout(drawerTimerIn);
    window.clearTimeout(drawerTimerOut);
    drawerTimerIn = window.setTimeout(function() {
      navItemHoverIn($navItem);
    }, delay);
  }, function() {
    clearTimeout(drawerTimerIn);
    drawerTimerOut = window.setTimeout(function() {
      navItemHoverOut($navItem);
    }, delay);
  });

  function navItemHoverIn(item) {
    window.clearTimeout(drawerTimerIn);
    window.clearTimeout(drawerTimerOut);
    drawer = $("[data-drawer="+item.attr('id')+"]");
    if (!open) {

      openDrawer(drawer)

    } else {
      current.hide();
      current = $('[data-drawer='+item.attr('id')+']');
      current.show();

    }

  }

  function navItemHoverOut(item) {
    window.clearTimeout(drawerTimerIn);
    window.clearTimeout(drawerTimerOut);
    closeDrawer();
  }

  $('#site-drawer').hover(function() {
    window.clearTimeout(drawerTimerIn);
    window.clearTimeout(drawerTimerOut);
  }, function() {
    drawerTimerIn = setTimeout(function() {
      closeDrawer();
    }, delay);

  });

  function closeDrawer() {
    $('#site-drawer').slideUp('fast').removeClass('shadow');

    if (current)
      current.fadeOut('fast');

    open = false;
  }

  function openDrawer(drawer) {
    current = drawer;
    current.show();
    $('#site-drawer').slideDown(200).addClass('shadow');
    open = true;
  }

})(jQuery);

// jQuery(document).ready(function($) {
//   Drupal.behaviors.drawerLinks();
// });