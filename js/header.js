(function($) {
  var $menu_icon = $('.menu-icon');
  var $teh_icon = $menu_icon.children('i');

  $menu_icon.click(function() {
    $('.main-navigation').toggleClass('open');
    toggleIcon();
  });

  function toggleIcon() {
    if ($teh_icon.hasClass('fa-bars')) {
      $teh_icon.removeClass('fa-bars').addClass('fa-times');
    } else {
      $teh_icon.removeClass('fa-times').addClass('fa-bars');
    }
  }

  var $icon = null;
  
  $icon = $('.section-menu').children('i');
  var $dropdown = $('.dropdown').find('.more');
//  console.log("section-menu found");

  var timer;
  $icon.click(function() {
    $('.horizontal-nav').toggleClass('open');
    toggleIcon();
  })
  $('.dropdown').click(function(event) {

  });
  $('.dropdown').hover(function() {
    window.clearTimeout(timer);
    $dropdown.slideDown('fast');
  }, function() {
    timer = setTimeout(function() {
      $dropdown.slideUp('fast');
    }, 500);
    
  });

  function toggleIcon() {
    if ($icon) {
//      console.log($icon);
      if ($icon.hasClass('fa-chevron-down')) {
        $icon.removeClass('fa-chevron-down').addClass('fa-chevron-up');
      } else {
        $icon.removeClass('fa-chevron-up').addClass('fa-chevron-down');
      }
    }
  }
})(jQuery);

//mustard cutting
(function($) {
  if (Modernizr.flexbox) {
    $('.popular-links').addClass('flex');
    $('.drawer-popular-links').addClass('flex');
    $('.global-nav').addClass('flex');
  }
})(jQuery);