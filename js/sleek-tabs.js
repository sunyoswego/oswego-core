(function($) {
  if ($('.sleek-tabs').length) {
    //$('.tab-pane').hide();
    //$('.tab-pane.first').show();
    var $el, leftPos, newWidth,
        $mainNav = $(".sleek-tabs ul");
    $mainNav.append("<li id='magic-line'></li>");
    var $magicLine = $("#magic-line");
    
    $magicLine
        .width($(".current").width())
        .css("left", $(".current a").position().left)
        .data("origLeft", $magicLine.position().left)
        .data("origWidth", $magicLine.width());
        
    $(".sleek-tabs-anchor").click(function(e) {
        e.preventDefault();    
        $el = $(this);
        var tab = $(this).attr('data-tab');
        $el.parent('li').siblings('li').each(function() {
          $(this).removeClass('current');
        });
        $el.parent('li').addClass('current');
        leftPos = $el.position().left;
        newWidth = $el.parent().width();
        $magicLine.stop().animate({
            left: leftPos,
            width: newWidth
        });

    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      // console.log(e.target); // newly activated tab
      // console.log(e.relatedTarget); // previous active tab
      if ($(window).width() < 768) {
        $('html,body').animate({scrollTop: $($(e.target).attr('href')).offset().top - $('.section-header-container').height()}, 2000);
      }
    })

    // $('.sleek-tabs a').click(function (e) {
    //   console.log(window.width());
    //   if (window.width() < 800) {
    //     console.log("TRUE SON");
    //   } else {
    //     e.preventDefault();
    //     $(this).parent('.tab').siblings('.tab').children('a').each(function() {
    //       var goAway = $(this).attr('data-tab');
    //       $(goAway).hide();
    //     });

    //     var tab = $(this).attr('data-tab');
    //     $(tab).show();
    //   }
      
    // });

    // var biggest = 0;

    // $('.tab-pane').each(function(index, el) {
    //   if ($(el).height() > biggest) {
    //     biggest = $(el).height();
    //   }
    // });

    // $('.tab-content').height(biggest + 100);

    $('.mobile-sleek-tabs-title').click(function(event) {
      $('.sleek-tabs ul').toggle();
    });

  }

  
})(jQuery);

    
  